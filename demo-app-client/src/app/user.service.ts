import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Http, Response, Headers, URLSearchParams, RequestOptions} from "@angular/http";
import {User} from "./user";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";

@Injectable()
export class UserService {

  constructor(private http:Http) { }

  userURL = "http://localhost:10080/api/user/login";
  allUsersURL = "http://localhost:10080/api/users";

  /**
   *
   * @param user
   * @returns {Observable<R|T>}
   */
  createUser(user: User):Observable<number> {
    let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: cpHeaders });
    return this.http.post(this.userURL, user, options)
      .map(success => success.status)
      .catch(this.handleError);
  }

  /**
   *
   * @param user
   * @returns {Observable<R|T>}
   */
  updateUser(user: User):Observable<number> {
    let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: cpHeaders });
    return this.http.put(this.userURL, user, options)
      .map(success => success.status)
      .catch(this.handleError);
  }

  /**
   *
   * @returns {Observable<R|T>}
   */
  getAllUsers(): Observable<User[]> {
    return this.http.get(this.allUsersURL)
      .map(this.extractData)
      .catch(this.handleError);
  }

  /**
   *
   * @param res
   * @returns {any}
   */
  private extractData(res: Response) {
    let body = res.json();
    return body;
  }

  /**
   *
   * @param error
   * @returns {any}
   */
  private handleError (error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.status);
  }


}
