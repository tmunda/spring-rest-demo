import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import {UserService} from "./user.service";
import {HttpModule} from "@angular/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {RouteNameService} from "./service/route-name.service";
import {CommonModule} from "@angular/common";
import { PageComponent } from './page/page.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    PageComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    RouterModule.forRoot([
      {
        path: 'user',
        component: UserComponent
      }, {
          path: 'page',
          component: PageComponent
      }
    ])
  ],
  exports: [
    FormsModule,//aqui
    ReactiveFormsModule//aqui
  ],

  providers: [
    UserService,
    RouteNameService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
