import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import {User} from "../user";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  //Properties
  allUsers: User[];
  statusCode: number;

  requestProcessing = false;
  userIdToUpdate = null;
  processValidation = false;

  //Create form
  userForm = new FormGroup({
    username: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  constructor(private userService: UserService,  private router: Router) { }

  ngOnInit() {
    this.getAllUsers();
  }

  private getAllUsers() {
    this.userService.getAllUsers()
      .subscribe(
        data => this.allUsers=data,
        errorCode => this.statusCode = errorCode);
  }

  private onUserFormSubmit() {
    this.processValidation = true;
    console.log("we are here");

    //Form is valid, now perform create or update
    this.preProcessConfigurations();
    let username = this.userForm.get('username').value;
    let phone = this.userForm.get('phone').value;
    let password = this.userForm.get('password').value;

    if (this.userIdToUpdate === null) {
      console.log("create user::");
      //Handle create user
      let user= new User(null, username, phone, password);

      this.userService.createUser(user)
        .subscribe(successCode => {
            this.statusCode = successCode;
            this.getAllUsers();
            this.backToCreateArticle();
          },
          errorCode => this.statusCode = errorCode);
    } else {
      //Handle update user
      let user= new User(this.userIdToUpdate,username ,phone, password);
      this.userService.updateUser(user)
        .subscribe(successCode => {
            this.statusCode = successCode;
            this.getAllUsers();
            this.backToCreateArticle();
          },
          errorCode => this.statusCode = errorCode);
    }
  }

  /**
   * When user clicks sign in button.
   */
  public onClickSignIn() {
    this.router.navigate([
      `/page`
    ]);
  }

  //Perform preliminary processing configurations
  preProcessConfigurations() {
    this.statusCode = null;
    this.requestProcessing = true;
  }
  //Go back from update to create
  backToCreateArticle() {
    this.userIdToUpdate = null;
    this.userForm.reset();
    // this.processValidation = false;
  }

}
