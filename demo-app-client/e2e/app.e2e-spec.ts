import { DemoAppClientPage } from './app.po';

describe('demo-app-client App', () => {
  let page: DemoAppClientPage;

  beforeEach(() => {
    page = new DemoAppClientPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
