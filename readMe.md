## To run App :

## Requirements:
* Ensure Tomcat server is installed and running.
* Build using maven.
* Ensure you have angular 4 installed. 

1) Run `mvn clean install`

2) After the build has completed successfully, deploy the `SpringRest-WAR` snapshot to the tomcat server I used version 7.0.9.
   The server will be running at `http://localhost:10080/`

3) Navigate to the demo-app-client directory, run `npm install`.

4) Run ng-serve this will launch the client. 

5) Navigate to http://localhost:4200/user to get to the landing page of the client.