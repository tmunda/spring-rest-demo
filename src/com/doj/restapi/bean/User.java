/**
 * 
 */
package com.doj.restapi.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Tinashe Munda
 *
 */
@Entity
@Table
public class User implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	public Long id;
	public String username;
	public String phone;
	public String password;
	public Integer token;
	
	public User() {
		super();
	}
	public User(String username, String phone, String password, Integer token) {
		super();
		this.username = username;
		this.phone = phone;
		this.password = password;
		this.token = token;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setToken(Integer token) { this.token = token;}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", phone=" + phone + ", password=" + password + "]";
	}
	
}
