/**
 * 
 */
package com.doj.restapi.web.controller;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.doj.restapi.bean.User;
import com.doj.restapi.service.IUserService;

/**
 * @author Tinashe Munda
 *
 */
@RestController
@CrossOrigin(origins = {"http://localhost:4200"})
public class UserController {
	
	@Autowired
	IUserService userService;

	@GetMapping("/")
	public String home (){
		return "Server Running!!!";
	}
	
	@GetMapping("/api/users")
	public List<User> all (){
		return userService.list();
	}

	@PostMapping("/api/user/login")
	public User create (@RequestBody User user){

		return userService.create(user);
	}

	@PutMapping("/api/user/add/{id}")
	public User update (@RequestBody User user, @PathVariable Long id){
		return userService.update(user, id);
	}


}
