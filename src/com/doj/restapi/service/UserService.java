/**
 * 
 */
package com.doj.restapi.service;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.doj.restapi.bean.User;
import com.doj.restapi.repository.UserRepository;

/**
 * @author Tinashe Munda
 *
 */
@Service
public class UserService implements IUserService {
	
	@Autowired
	UserRepository userRepository;
	
	@Override
	public User create(User user) {
		Random rand = new Random();
		int  n = rand.nextInt(10000000) + 1;
		user.setToken(n);
		return userRepository.save(user);
	}

	@Override
	public User get(Long id) {
		return userRepository.findOne(id);
	}

	@Override
	public User update(User user, Long id) {
		return userRepository.save(user);
	}

	@Override
	public void delete(Long id) {
		userRepository.delete(id);
	}

	@Override
	public List<User> list() {
		return (List<User>) userRepository.findAll();
	}
}
