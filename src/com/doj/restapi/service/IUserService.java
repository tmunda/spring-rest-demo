/**
 * 
 */
package com.doj.restapi.service;

import java.util.List;

import com.doj.restapi.bean.User;

/**
 * @author Tinashe Munda
 *
 */
public interface IUserService {
	
	User create(User user);
	User get(Long id);
	List<User> list();
	User update(User user, Long id);
	void delete(Long id);
}
