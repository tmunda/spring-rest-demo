/**
 * 
 */
package com.doj.restapi.repository;

import org.springframework.data.repository.CrudRepository;

import com.doj.restapi.bean.User;

/**
 * @author Tinashe Munda
 *
 */
public interface UserRepository extends CrudRepository<User, Long> {

}
